<?php


class SFConfigTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  \SFConfig\SFConfig */
    protected $config;

    protected $configDir;

    protected function _before()
    {
        $this->configDir = __DIR__ . '/../_data';
        $this->config    = new \SFConfig\SFConfig('api.test.safechats.com', [], [], $this->configDir);
    }

    public function testSupportedDomains()
    {
        new \SFConfig\SFConfig('api.safechats.com');
        new \SFConfig\SFConfig('api.dev.safechats.com');

        $this->setExpectedException('\SFConfig\Exception\NotSupportedDomainException');
        new \SFConfig\SFConfig('api.not_supported.safechats.com');
    }

    public function testGetParam()
    {
        $this->assertEquals('array field value', $this->config->getParam('array>field'));
        $this->assertEquals('inner field value', $this->config->getParam('array > inner > field'));
    }

    public function testGetConfig()
    {
        $params = $this->config->getConfig();

        $this->assertEquals('array field value', $params['array']['field']);
        $this->assertEquals('inner field value', $params['array']['inner']['field']);
    }

    public function testRequiredParams()
    {
        $this->setExpectedException('\SFConfig\Exception\RequiredParamsException');

        $requiredParams = [
            'array > inner > field',
            'array > inner > not_exists_field'
        ];
        $this->config   = new \SFConfig\SFConfig('api.test.safechats.com', $requiredParams, [], $this->configDir);
    }

    public function testExtraValues()
    {
        $requiredParams = ['extra'];
        $extraValues    = [
            'extra' => 'value',
            'array' => [
                'inner' => [
                    'field' => 'extra value'
                ]
            ]
        ];
        $this->config   = new \SFConfig\SFConfig(
            'api.test.safechats.com',
            $requiredParams,
            $extraValues,
            $this->configDir
        );

        $params = $this->config->getConfig();

        $this->assertEquals('value', $params['extra']);
        $this->assertEquals('extra value', $params['array']['inner']['field']);

        $this->assertEquals('array field value', $params['array']['field']);
    }

}