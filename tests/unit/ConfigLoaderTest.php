<?php


class ConfigLoaderTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /** @var  \SFConfig\ConfigLoader */
    protected $configLoader;

    protected function _before()
    {
        $this->configLoader = new \SFConfig\ConfigLoader();
        $this->configLoader->setConfigDir(__DIR__ . '/../_data');
    }

    public function testLoad()
    {
        $this->configLoader->setDomain('api.test.safechats.com');
        $configParams = $this->configLoader->load();

        $this->assertArrayHasKey('domain', $configParams);
        $this->assertEquals('api.test.safechats.com', $configParams['domain']);
    }

    public function testLoadThrowExceptionWhenConfigurationFileNotFound()
    {
        $this->setExpectedException('\SFConfig\Exception\ConfigurationFileNotFoundException');

        $this->configLoader->setDomain('api.not_found.safechats.com');
        $this->configLoader->load();
    }

    public function testLoadThrowExceptionWhenConfigurationParsingFailed()
    {
        $this->setExpectedException('\SFConfig\Exception\ConfigurationFileParsingException');

        $this->configLoader->setDomain('api.not_json.safechats.com');
        $this->configLoader->load();
    }

}