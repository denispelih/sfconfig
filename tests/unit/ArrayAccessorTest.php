<?php


class ArrayAccessorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    
    /** @var  \SFConfig\ArrayAccessor */
    protected $arrayAccessor;

    protected $data = [
        'key'   => 'value',
        'inner' => [
            'key'   => 'inner value',
            'inner' => [
                'key' => 'double inner value'
            ],
            'key with space' => 'space'
        ]
    ];

    protected function _before()
    {
        $this->arrayAccessor = new \SFConfig\ArrayAccessor();
        $this->arrayAccessor->setData($this->data);
    }

    public function testHasKey()
    {
        $this->assertTrue($this->arrayAccessor->hasKey('key'));
        $this->assertTrue($this->arrayAccessor->hasKey('inner>key'));
        $this->assertTrue($this->arrayAccessor->hasKey('inner > inner > key'));

        $this->assertFalse($this->arrayAccessor->hasKey('inner > failed_key'));
    }

    public function testHasKeyUsingCache()
    {
        $this->assertTrue($this->arrayAccessor->hasKey('inner > inner > key'));
        $this->assertTrue($this->arrayAccessor->hasKey('inner > inner > key'));
        $this->assertTrue($this->arrayAccessor->hasKey('inner>inner>key'));

        $this->assertFalse($this->arrayAccessor->hasKey('inner > failed_key'));
        $this->assertFalse($this->arrayAccessor->hasKey('inner > failed_key'));
        $this->assertFalse($this->arrayAccessor->hasKey('inner>failed_key'));
    }

    public function testGetValue()
    {
        $this->assertEquals('value', $this->arrayAccessor->getValue('key'));
        $this->assertEquals('inner value', $this->arrayAccessor->getValue('inner>key'));
        $this->assertEquals('double inner value', $this->arrayAccessor->getValue('inner > inner > key'));
    }

    public function testGetValueUsingCache()
    {
        $this->assertEquals('double inner value', $this->arrayAccessor->getValue('inner > inner > key'));
        $this->assertEquals('double inner value', $this->arrayAccessor->getValue('inner > inner > key'));
        $this->assertEquals('double inner value', $this->arrayAccessor->getValue('inner>inner>key'));
    }

    public function testGetValueThrowExceptionWhenKeyNotExists()
    {
        $this->setExpectedException('\SFConfig\Exception\KeyNotExistsException');
        $this->arrayAccessor->getValue('inner > inner > not_exists_key');
    }
}