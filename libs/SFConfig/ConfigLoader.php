<?php


namespace SFConfig;


use SFConfig\Exception\ConfigurationFileNotFoundException;
use SFConfig\Exception\ConfigurationFileParsingException;

/**
 * Loading, parsing and comments deleting configuration file.
 */
class ConfigLoader
{
    private $configDir;
    private $domain;

    /**
     * Get default path to configuration directory
     *
     * @return string
     */
    static public function getDefaultConfigDir()
    {
        return __DIR__ . '/../../conf';
    }

    public function __construct()
    {
        $this->setConfigDir(static::getDefaultConfigDir());
    }

    /**
     * @param string $configDir     Path to configuration directory
     */
    public function setConfigDir($configDir)
    {
        $this->configDir = $configDir;
    }

    /**
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * Loading, parsing and comments deleting configuration file.
     *
     * @return array    All configuration parameters as array
     * @throws ConfigurationFileNotFoundException
     * @throws ConfigurationFileParsingException
     */
    public function load()
    {
        if (!file_exists($this->getFileName())) {
            throw new ConfigurationFileNotFoundException("Configuration file for {$this->domain} not found");
        }

        $content = file_get_contents($this->getFileName());
        $content = $this->clearComments($content);

        $configParameters = json_decode($content, true);

        if (json_last_error()) {
            $errorMsg = json_last_error_msg();
            throw new ConfigurationFileParsingException(
                "Configuration file parsing exception with message: {$errorMsg}"
            );
        }

        return $configParameters;
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return "{$this->configDir}/{$this->domain}.conf";
    }

    /**
     * Deleting all comments from json-string.
     *
     * Comment is all characters in string after "#" character
     *
     * @param string $content  Configuration file content
     *
     * @return string
     */
    private function clearComments($content)
    {
        return preg_replace('/#.*$/m', '', $content);
    }
}