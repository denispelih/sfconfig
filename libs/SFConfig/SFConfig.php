<?php


namespace SFConfig;


use SFConfig\Exception\NotSupportedDomainException;
use SFConfig\Exception\RequiredParamsException;

/**
 * SFConfig is the class for configuration parameters managing and getting from configuration files.
 */
class SFConfig
{
    private $supportedDomains = [
        'api.safechats.com',
        'api.dev.safechats.com',
        'api.test.safechats.com',
    ];

    /** @var  ConfigLoader */
    private $configLoader;
    /** @var  ArrayAccessor */
    private $arrayAccessor;

    private $extraValues;
    private $config;

    /**
     * @param string $domain
     * @param array $requiredParams
     * @param array $extraValues        Values of additional configuration parameters
     * @param string $configDir         Path to configuration directory
     *
     * @throws NotSupportedDomainException
     * @throws RequiredParamsException
     */
    public function __construct(
        $domain,
        array $requiredParams = [],
        array $extraValues    = [],
        $configDir            = '')
    {
        $this->extraValues = $extraValues;

        $this->initConfigLoader($domain, $configDir);
        $this->initArrayAccessor();
        $this->checkSupportedDomain($domain);

        if (!empty($requiredParams)) {
            $this->checkRequiredParams($requiredParams);
        }
    }

    /**
     * Get value configuration parameter
     *
     * @param string $key   Key is string in format "outer_key > inner_key > key > ...". ">" is delimiter for designation one level down.
     *
     * @return array|null
     * @throws Exception\KeyNotExistsException
     */
    public function getParam($key)
    {
        $this->lazyLoadConfig();

        return $this->arrayAccessor->getValue($key);
    }

    /**
     * Get all parameters as associative array.
     *
     * @return array|null
     */
    public function getConfig()
    {
        $this->lazyLoadConfig();

        return $this->config;
    }

    /**
     * Creating and initialization <tt>ConfigLoader</tt>{@link ConfigLoader}
     *
     * @param string $domain
     * @param string $configDir    Path to configuration directory
     */
    private function initConfigLoader($domain, $configDir)
    {
        $this->configLoader = new ConfigLoader();
        $this->configLoader->setDomain($domain);
        if (!empty($configDir)) {
            $this->configLoader->setConfigDir($configDir);
        }
    }

    /**
     * Creating and initialization <tt>ArrayAccessor</tt>{@link ArrayAccessor}
     */
    private function initArrayAccessor()
    {
        $this->arrayAccessor = new ArrayAccessor();
    }

    /**
     * @param string $domain
     *
     * @throws NotSupportedDomainException
     */
    private function checkSupportedDomain($domain)
    {
        if (!$this->isSupportedDomain($domain)) {
            throw new NotSupportedDomainException();
        }
    }

    /**
     * @param string $domain
     *
     * @return bool
     */
    private function isSupportedDomain($domain)
    {
        return in_array($domain, $this->supportedDomains);
    }

    /**
     * @param array $requiredParams
     *
     * @throws RequiredParamsException
     */
    private function checkRequiredParams(array $requiredParams)
    {
        $this->lazyLoadConfig();

        foreach ($requiredParams as $param) {
            if (!$this->arrayAccessor->hasKey($param)) {
                throw new RequiredParamsException("Required parameter '{$param}' is not set");
            }
        }

    }

    /**
     * This method use for lazy loading configuration
     *
     * @throws Exception\ConfigurationFileNotFoundException
     * @throws Exception\ConfigurationFileParsingException
     */
    private function lazyLoadConfig()
    {
        if (is_null($this->config)) {
            $this->config = $this->configLoader->load();
            $this->config = array_replace_recursive($this->config, $this->extraValues);
            $this->arrayAccessor->setData($this->config);
        }
    }
}