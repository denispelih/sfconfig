<?php


namespace SFConfig;


use SFConfig\Exception\KeyNotExistsException;

/**
 * Array access on key in format "outer_key > inner_key > key > ...".
 */
class ArrayAccessor
{
    /**
     * @var array data
     */
    private $data  = [];
    /**
     * @var array cache
     */
    private $cache = [];

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data  = $data;
        $this->cache = [];
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function hasKey($key)
    {
        $value = $this->process($key);

        return !empty($value);
    }

    /**
     * @param string $key
     *
     * @return array|null
     * @throws KeyNotExistsException
     */
    public function getValue($key)
    {
        $value = $this->process($key);

        if (is_null($value)) {
            throw new KeyNotExistsException("Key '{$key}' not exists");
        }

        return $value;
    }

    /**
     * Get value for key from data and save to cache
     *
     * @param string $key   Key is string in format "outer_key > inner_key > key > ...". ">" is delimiter for designation one level down.
     *
     * @return array|null
     */
    private function process($key)
    {
        $key = str_replace(' > ', '>', $key);
        if (array_key_exists($key, $this->cache)) {
            return $this->cache[$key];
        }

        $keys  = explode('>', $key);
        $value = $this->data;

        foreach($keys as $current) {
            if (!isset($value[$current])) {
                $this->cache[$key] = null;
                return null;
            }
            $value = $value[$current];
        }

        $this->cache[$key] = $value;

        return $value;
    }

}